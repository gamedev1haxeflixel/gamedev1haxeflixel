package lime;


import lime.utils.Assets;


class AssetData {

	private static var initialized:Bool = false;
	
	public static var library = new #if haxe3 Map <String, #else Hash <#end LibraryType> ();
	public static var path = new #if haxe3 Map <String, #else Hash <#end String> ();
	public static var type = new #if haxe3 Map <String, #else Hash <#end AssetType> ();	
	
	public static function initialize():Void {
		
		if (!initialized) {
			
			path.set ("assets/data/80.csv", "assets/data/80.csv");
			type.set ("assets/data/80.csv", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/data-goes-here.txt", "assets/data/data-goes-here.txt");
			type.set ("assets/data/data-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/GD1_HFC01.oep", "assets/data/GD1_HFC01.oep");
			type.set ("assets/data/GD1_HFC01.oep", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/GD1_HFC02.oep", "assets/data/GD1_HFC02.oep");
			type.set ("assets/data/GD1_HFC02.oep", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/GD1_Proj1 - Shortcut.lnk", "assets/data/GD1_Proj1 - Shortcut.lnk");
			type.set ("assets/data/GD1_Proj1 - Shortcut.lnk", Reflect.field (AssetType, "binary".toUpperCase ()));
			path.set ("assets/data/Stage00.oel", "assets/data/Stage00.oel");
			type.set ("assets/data/Stage00.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/Stage01.oel", "assets/data/Stage01.oel");
			type.set ("assets/data/Stage01.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/Stage02.oel", "assets/data/Stage02.oel");
			type.set ("assets/data/Stage02.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/Stage03.oel", "assets/data/Stage03.oel");
			type.set ("assets/data/Stage03.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/testTiling.csv", "assets/data/testTiling.csv");
			type.set ("assets/data/testTiling.csv", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/background-Blue&Yellow.png", "assets/images/background-Blue&Yellow.png");
			type.set ("assets/images/background-Blue&Yellow.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background-Blue.png", "assets/images/background-Blue.png");
			type.set ("assets/images/background-Blue.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background.png", "assets/images/background.png");
			type.set ("assets/images/background.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background1N.png", "assets/images/background1N.png");
			type.set ("assets/images/background1N.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background2N.png", "assets/images/background2N.png");
			type.set ("assets/images/background2N.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background3N.png", "assets/images/background3N.png");
			type.set ("assets/images/background3N.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background4N.png", "assets/images/background4N.png");
			type.set ("assets/images/background4N.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/backgroundBlue.png", "assets/images/backgroundBlue.png");
			type.set ("assets/images/backgroundBlue.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/backgroundGrey.png", "assets/images/backgroundGrey.png");
			type.set ("assets/images/backgroundGrey.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Backgroundsky.png", "assets/images/Backgroundsky.png");
			type.set ("assets/images/Backgroundsky.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/backgroundYellow.png", "assets/images/backgroundYellow.png");
			type.set ("assets/images/backgroundYellow.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background_Color.png", "assets/images/background_Color.png");
			type.set ("assets/images/background_Color.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/background_Gray.png", "assets/images/background_Gray.png");
			type.set ("assets/images/background_Gray.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/bgColor.png", "assets/images/bgColor.png");
			type.set ("assets/images/bgColor.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/BigBadAll.json", "assets/images/BigBadAll.json");
			type.set ("assets/images/BigBadAll.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/BigBadAll.png", "assets/images/BigBadAll.png");
			type.set ("assets/images/BigBadAll.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Blue.png", "assets/images/Blue.png");
			type.set ("assets/images/Blue.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/BluePlayerAll.json", "assets/images/BluePlayerAll.json");
			type.set ("assets/images/BluePlayerAll.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/BluePlayerAll.png", "assets/images/BluePlayerAll.png");
			type.set ("assets/images/BluePlayerAll.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Concept.png", "assets/images/Concept.png");
			type.set ("assets/images/Concept.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Enemy1All.json", "assets/images/Enemy1All.json");
			type.set ("assets/images/Enemy1All.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/Enemy1All.png", "assets/images/Enemy1All.png");
			type.set ("assets/images/Enemy1All.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Enemy1Jump.png", "assets/images/Enemy1Jump.png");
			type.set ("assets/images/Enemy1Jump.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Enemy2.png", "assets/images/Enemy2.png");
			type.set ("assets/images/Enemy2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Enemy2All.json", "assets/images/Enemy2All.json");
			type.set ("assets/images/Enemy2All.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/Enemy2All.png", "assets/images/Enemy2All.png");
			type.set ("assets/images/Enemy2All.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/factory.png", "assets/images/factory.png");
			type.set ("assets/images/factory.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/images-go-here.txt", "assets/images/images-go-here.txt");
			type.set ("assets/images/images-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/inside_tile_new.png", "assets/images/inside_tile_new.png");
			type.set ("assets/images/inside_tile_new.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/machine.png", "assets/images/machine.png");
			type.set ("assets/images/machine.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/machine1.png", "assets/images/machine1.png");
			type.set ("assets/images/machine1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/machine2.png", "assets/images/machine2.png");
			type.set ("assets/images/machine2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/machine3.png", "assets/images/machine3.png");
			type.set ("assets/images/machine3.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/platforms.png", "assets/images/platforms.png");
			type.set ("assets/images/platforms.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/PlayerAll.json", "assets/images/PlayerAll.json");
			type.set ("assets/images/PlayerAll.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/PlayerAll.png", "assets/images/PlayerAll.png");
			type.set ("assets/images/PlayerAll.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/PlayerJump.png", "assets/images/PlayerJump.png");
			type.set ("assets/images/PlayerJump.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/PlayerRun.png", "assets/images/PlayerRun.png");
			type.set ("assets/images/PlayerRun.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Red.png", "assets/images/Red.png");
			type.set ("assets/images/Red.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/RedPlayerAll.json", "assets/images/RedPlayerAll.json");
			type.set ("assets/images/RedPlayerAll.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/RedPlayerAll.png", "assets/images/RedPlayerAll.png");
			type.set ("assets/images/RedPlayerAll.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/StartButton.png", "assets/images/StartButton.png");
			type.set ("assets/images/StartButton.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Syringe.png", "assets/images/Syringe.png");
			type.set ("assets/images/Syringe.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/tilemap.png", "assets/images/tilemap.png");
			type.set ("assets/images/tilemap.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/tile_2.png", "assets/images/tile_2.png");
			type.set ("assets/images/tile_2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/tile_inside.png", "assets/images/tile_inside.png");
			type.set ("assets/images/tile_inside.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/tile_outside.png", "assets/images/tile_outside.png");
			type.set ("assets/images/tile_outside.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Title.png", "assets/images/Title.png");
			type.set ("assets/images/Title.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Yellow.png", "assets/images/Yellow.png");
			type.set ("assets/images/Yellow.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/YellowPlayerAll.json", "assets/images/YellowPlayerAll.json");
			type.set ("assets/images/YellowPlayerAll.json", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/YellowPlayerAll.png", "assets/images/YellowPlayerAll.png");
			type.set ("assets/images/YellowPlayerAll.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/music/AllColors.wav", "assets/music/AllColors.wav");
			type.set ("assets/music/AllColors.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/Color1.wav", "assets/music/Color1.wav");
			type.set ("assets/music/Color1.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/Color2.wav", "assets/music/Color2.wav");
			type.set ("assets/music/Color2.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/Colors0.wav", "assets/music/Colors0.wav");
			type.set ("assets/music/Colors0.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/music-goes-here.txt", "assets/music/music-goes-here.txt");
			type.set ("assets/music/music-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/sounds/BlueColorFound.mp3", "assets/sounds/BlueColorFound.mp3");
			type.set ("assets/sounds/BlueColorFound.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/BlueColorFound.wav", "assets/sounds/BlueColorFound.wav");
			type.set ("assets/sounds/BlueColorFound.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/BlueColorReturned.mp3", "assets/sounds/BlueColorReturned.mp3");
			type.set ("assets/sounds/BlueColorReturned.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/BlueColorReturned.wav", "assets/sounds/BlueColorReturned.wav");
			type.set ("assets/sounds/BlueColorReturned.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/GradyBeginLine.mp3", "assets/sounds/GradyBeginLine.mp3");
			type.set ("assets/sounds/GradyBeginLine.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/GradyBeginLine.wav", "assets/sounds/GradyBeginLine.wav");
			type.set ("assets/sounds/GradyBeginLine.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/GradyEndLine.mp3", "assets/sounds/GradyEndLine.mp3");
			type.set ("assets/sounds/GradyEndLine.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/GradyEndLine.wav", "assets/sounds/GradyEndLine.wav");
			type.set ("assets/sounds/GradyEndLine.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Introduction.mp3", "assets/sounds/Introduction.mp3");
			type.set ("assets/sounds/Introduction.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/Introduction.wav", "assets/sounds/Introduction.wav");
			type.set ("assets/sounds/Introduction.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/JumpHit.wav", "assets/sounds/JumpHit.wav");
			type.set ("assets/sounds/JumpHit.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/Jumpsound.wav", "assets/sounds/Jumpsound.wav");
			type.set ("assets/sounds/Jumpsound.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/RedColorFound.mp3", "assets/sounds/RedColorFound.mp3");
			type.set ("assets/sounds/RedColorFound.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/RedColorFound.wav", "assets/sounds/RedColorFound.wav");
			type.set ("assets/sounds/RedColorFound.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/RedColorReturned.mp3", "assets/sounds/RedColorReturned.mp3");
			type.set ("assets/sounds/RedColorReturned.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/sounds-go-here.txt", "assets/sounds/sounds-go-here.txt");
			type.set ("assets/sounds/sounds-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/sounds/YellowColorFound.mp3", "assets/sounds/YellowColorFound.mp3");
			type.set ("assets/sounds/YellowColorFound.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/YellowColorFound.wav", "assets/sounds/YellowColorFound.wav");
			type.set ("assets/sounds/YellowColorFound.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/YellowColorReturned.mp3", "assets/sounds/YellowColorReturned.mp3");
			type.set ("assets/sounds/YellowColorReturned.mp3", Reflect.field (AssetType, "music".toUpperCase ()));
			path.set ("assets/sounds/YellowColorReturned.wav", "assets/sounds/YellowColorReturned.wav");
			type.set ("assets/sounds/YellowColorReturned.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("flixel/sounds/beep.ogg", "flixel/sounds/beep.ogg");
			type.set ("flixel/sounds/beep.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("flixel/sounds/flixel.ogg", "flixel/sounds/flixel.ogg");
			type.set ("flixel/sounds/flixel.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("flixel/fonts/nokiafc22.ttf", "flixel/fonts/nokiafc22.ttf");
			type.set ("flixel/fonts/nokiafc22.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
			path.set ("flixel/fonts/monsterrat.ttf", "flixel/fonts/monsterrat.ttf");
			type.set ("flixel/fonts/monsterrat.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
			path.set ("flixel/images/ui/button.png", "flixel/images/ui/button.png");
			type.set ("flixel/images/ui/button.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("flixel/images/logo/default.png", "flixel/images/logo/default.png");
			type.set ("flixel/images/logo/default.png", Reflect.field (AssetType, "image".toUpperCase ()));
			
			
			initialized = true;
			
		} //!initialized
		
	} //initialize
	
	
} //AssetData
