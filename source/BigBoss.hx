package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import flixel.system.FlxSound;
import Enemy;

class BigBoss extends Enemy{ //Class for the game's flying enemy

	var _animationLock:Bool = false;
	var _begin:FlxSound;
	var _end:FlxSound;

	public function new(xPos:Int = 1000, yPos:Int = 1520, xL:Int = 1000,xR:Int = 1700){
		super();

		_begin = FlxG.sound.load(AssetPaths.GradyBeginLine__wav);
		_end = FlxG.sound.load(AssetPaths.GradyEndLine__wav);

		x = xPos;
		y = yPos;

		xRight = xR;
		xLeft = xL;

		//Change sprite size
		scale.set(0.8,0.8);

		width = 50;
		height = 50;

		var spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/BigBadAll.png","assets/images/BigBadAll.json");
		frames = spritesheet;
		animation.addByPrefix("Idle","BigBadEvil_Idle",20,true,true,false);
		animation.addByPrefix("Nice","BigBadNice_",1,false,true,false);

		setFacingFlip(FlxObject.RIGHT, true, false);
		setFacingFlip(FlxObject.LEFT, false, false);
		//animation.add("walk", [0,1,0,2], 10, true);
		drag.x = 1000;
		maxVelocity.set(400, 1000);

		//_begin.play();
	}

	override public function update(elapsed:Float):Void{
		checkFacing();
		animate();
		movement();

		if(y <= -500) {
			FlxG.switchState(new MenuState());
			kill();
		}

		super.update(elapsed);
	}

	function checkFacing():Void {
		if(!_animationLock) {
			if(x>xRight) {
				currentDirection = "RIGHT";
				_rot = 180;
				facing  = FlxObject.RIGHT;
				acceleration.x = 0;
			}
			else if(x<xLeft) {
				currentDirection = "LEFT";
				_rot = 0;
				facing  = FlxObject.LEFT;
				acceleration.x = 0;
			}
		}
	}

	function movement():Void{
		if(!_animationLock) {
			if (currentDirection == "RIGHT") {
				acceleration.x -= drag.x;
			}
			else {
				acceleration.x += drag.x;
			}
		}
	}

	function animate():Void{
		if(!_animationLock) {
			if(velocity.x != 0 && velocity.y == 0) {
				animation.play("Idle");
			}
			else {
				animation.stop();
			}
		}
	}

	override public function die():Void {

		_animationLock = true;
		velocity.x = 0;
		acceleration.x = 0;
		width = 0;
		height = 0;
		alive = false;
		acceleration.y = -300;
		animation.play("Nice");
		_end.play();
	}
}