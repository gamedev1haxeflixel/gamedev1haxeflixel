package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.addons.ui.FlxButtonPlus;
import flixel.FlxSprite;


class MenuState extends FlxState
{
	var _background:FlxSprite;
	var _title:FlxText;
	var _restartButton:FlxButton;
	
	override public function create():Void
	{
		super.create();
		
		
		_restartButton = new FlxButton(0,0, "", clickPlay);
		_restartButton.loadGraphic(AssetPaths.StartButton__png,false,142,136);
		_title = new FlxText(350,200, 600, "VICTORY!", 100);
		//_title.borderColor = FlxColor.RED;
		_background = new FlxSprite();
		_background.loadGraphic(AssetPaths.background_Color__png,false,1280,720);
		_background.y = 25;
		_restartButton.x = 550;
		_restartButton.y = 375;

		
		add(_background);
		add(_title);
		
		add(_restartButton);

		if (FlxG.sound.music == null) // don't restart the music if it's already playing
 		{
     		FlxG.sound.playMusic(AssetPaths.Colors0__wav, 1, true);
 		}
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	function clickPlay():Void{
		FlxG.switchState(new PlayState());
	}
}
