package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;

class Enemy extends FlxSprite{ //Generic for the game's enemy

	var speed:Float = 200;
	var _rot:Float = 0;
	var _up:Bool = false;
	var _down:Bool = false;
	var _left:Bool = false;
	var _right:Bool = false;

	//Set the x axis boundaries for how far the enemy will go
	var currentDirection:String = "RIGHT";
	var xLeft:Int;
	var xRight:Int;

	public function new(xPos:Int = 2980, yPos:Int = 1302, xL:Int = 3000,xR:Int = 3400) {
		super();

		xLeft = xL;
		xRight = xR;

		x = xPos;
		y = yPos;
	}

	public function die():Void {
		
	}

}