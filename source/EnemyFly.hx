package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import Enemy;

class EnemyFly extends Enemy{ //Class for the game's flying enemy

	var _animationLock:Bool = false;

	public function new(xPos:Int = 2980, yPos:Int = 1302, xL:Int = 3100,xR:Int = 3300){
		super();

		x = xPos;
		y = yPos;

		xRight = xR;
		xLeft = xL;

		//Change sprite size
		scale.set(0.8,0.8);

		width = 40;
		height = 125;
		offset.set(35,9);

		var spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/Enemy2All.png","assets/images/Enemy2All.json");
		frames = spritesheet;
		animation.addByPrefix("EnemyWalk","Enemy2Walk_",20,true,true,false);
		animation.addByPrefix("EnemyDeath","Enemy2Death_",8,false,true,false);

		setFacingFlip(FlxObject.RIGHT, true, false);
		setFacingFlip(FlxObject.LEFT, false, false);
		//animation.add("walk", [0,1,0,2], 10, true);
		drag.x = 1000;
		maxVelocity.set(400, 1000);
	}

	override public function update(elapsed:Float):Void{
		checkFacing();
		animate();
		movement();

		if(!alive && animation.finished) {
			kill();
		}

		super.update(elapsed);
	}

	function checkFacing():Void {
		if(!_animationLock) {
			if(x>xRight) {
				currentDirection = "RIGHT";
				_rot = 180;
				facing  = FlxObject.RIGHT;
				acceleration.x = 0;
			}
			else if(x<xLeft) {
				currentDirection = "LEFT";
				_rot = 0;
				facing  = FlxObject.LEFT;
				acceleration.x = 0;
			}
		}
	}

	function movement():Void{
		if(!_animationLock) {
			if (currentDirection == "RIGHT") {
				acceleration.x -= drag.x;
			}
			else {
				acceleration.x += drag.x;
			}
		}
	}

	function animate():Void{
		if(!_animationLock) {
			if(velocity.x != 0 && velocity.y == 0) {
				animation.play("EnemyWalk");
			}
			else {
				animation.stop();
			}
		}
	}

	override public function die():Void {

		_animationLock = true;
		velocity.x = 0;
		acceleration.x = 0;
		width = 0;
		height = 0;
		alive = false;
		animation.play("EnemyDeath");
	}
}