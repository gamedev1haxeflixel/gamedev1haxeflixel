package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import Enemy;

class EnemyGround extends Enemy{ //Class for the game's ground enemy

	var _animationLock:Bool = false;

	public function new(xPos:Int = 1400, yPos:Int = 1002, xL:Int = 1050,xR:Int = 1100){
		super();

		x = xPos;
		y = yPos;

		xRight = xR;
		xLeft = xL;

		//Change sprite size
		scale.set(0.8,0.8);

		width = 20;
		height = 10;
		offset.set(5,0);
		updateHitbox();

		var spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/Enemy1All.png","assets/images/Enemy1All.json");
		frames = spritesheet;
		animation.addByPrefix("EnemyWalk","Enemy1Walk_",10,true,true,false);
		animation.addByPrefix("EnemyDeath","Enemy1Death_",24,false,true,false);

		setFacingFlip(FlxObject.RIGHT, true, false);
		setFacingFlip(FlxObject.LEFT, false, false);
		//animation.add("walk", [0,1,0,2], 10, true);
		drag.x = 500;
		acceleration.y = 600;
		maxVelocity.set(250, 1000);
	}

	override public function update(elapsed:Float):Void{
		checkFacing();
		animate();
		movement();

		if(!alive && animation.finished) {
			kill();
		}

		super.update(elapsed);
	}

	function checkFacing():Void {
		if(!_animationLock) {
			if(x>=xRight && isTouching(FlxObject.DOWN)) {
				currentDirection = "RIGHT";
				_rot = 180;
				facing  = FlxObject.RIGHT;
				acceleration.x = 0;
			}
			else if(x<=xLeft && isTouching(FlxObject.DOWN)) {
				currentDirection = "LEFT";
				_rot = 0;
				facing  = FlxObject.LEFT;
				acceleration.x = 0;
			}
		}
	}

	function movement():Void{
		if(!_animationLock) {
			if (isTouching(FlxObject.DOWN)) {
				velocity.y = -300;
			}
			else if (currentDirection == "RIGHT" && velocity.y != 0) {
				acceleration.x -= drag.x;
			}
			else if (currentDirection == "LEFT" && velocity.y != 0) {
				acceleration.x += drag.x;
			}
		}
	}

	function animate():Void{
		if(!_animationLock) {
			if(velocity.x != 0) {
				animation.play("EnemyWalk");
			}
			else {
				animation.stop();
			}
		}
	}

	override public function die():Void {

		_animationLock = true;
		animation.stop();
		velocity.x = 0;
		acceleration.x = 0;
		width = 0;
		height = 0;
		alive = false;
		animation.play("EnemyDeath");
	}
}