package;

import flixel.FlxGame;
import openfl.display.Sprite;

class Main extends Sprite
{
	var GameWidth:Int = 3840;
	var GameHeight:Int = 1536;

	public function new()
	{
		super();
		addChild(new FlxGame(0, 0, MenuState));
	}
}
