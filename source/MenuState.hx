package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.addons.ui.FlxButtonPlus;
import flixel.FlxSprite;


class MenuState extends FlxState
{
	var _title:FlxSprite;
	var _background:FlxSprite;

	var _playButton:FlxButton;
	var _name1:FlxText;
	var _name2:FlxText;
	var _name3:FlxText;
	var _name4:FlxText;
	var _name5:FlxText;
	
	override public function create():Void
	{
		super.create();
		
		
		_playButton = new FlxButton(10,10, "", clickPlay);
		_playButton.loadGraphic(AssetPaths.StartButton__png,false,142,136);
		_background = new FlxSprite();
		_background.loadGraphic(AssetPaths.background1N__png,false,1280,720);
		_title = new FlxSprite();
		_title.loadGraphic(AssetPaths.Title__png, false, 843, 284);
		_background.y = 25;
		_title.x = 200;
		_title.y = 100;
		_playButton.x = 550;
		_playButton.y = 375;
		_name1 = new FlxText(525, 500, "Claire Thomas", 20);
		_name2 = new FlxText(525, 520, "Robert DeFelice", 20);
		_name3 = new FlxText(525, 540, "Matthew Chuang", 20);
		_name4 = new FlxText(525, 560, "Eve Eai", 20);
		_name5 = new FlxText(525, 580, "Daniel Hendricks", 20);

		
		add(_background);
		add(_title);
		add(_playButton);
		add(_name1);
		add(_name2);
		add(_name3);
		add(_name4);
		add(_name5);
		

		if (FlxG.sound.music == null) // don't restart the music if it's already playing
 		{
     		FlxG.sound.playMusic(AssetPaths.Colors0__wav, 1, true);
 		}
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	function clickPlay():Void{
		FlxG.switchState(new PlayState());
	}
}