package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.tile.FlxTilemap;
import openfl.Assets;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.FlxObject;
import flixel.group.FlxGroup;
import flixel.FlxSprite;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.util.FlxColor;
import flixel.system.FlxSound;

class PlayState extends FlxState
{
	var _player:Player;
	var _background:FlxSprite;
	var _machine:FlxSprite;
	var _element:FlxSprite;
	var _grpEnemies:FlxTypedGroup<Enemy>;

	var _jumpHit:FlxSound;
	var _introduction:FlxSound;
	var _blueFound:FlxSound;
	var _blueReturned:FlxSound;
	var _yellowFound:FlxSound;
	var _yellowReturned:FlxSound;
	var _redFound:FlxSound;
	var _redReturned:FlxSound;
	var _begin:FlxSound;

	var _phase:Int = 0; //Keeps track of the current game phase to load in new assets

	var tileMap:FlxTilemap;
	static var _mWalls:FlxTilemap;
	static var _bg:FlxTilemap;
	static var _bg1:FlxSprite;
	static var _bg2:FlxTilemap;
	static var _map:FlxOgmoLoader;
	static var _boss:FlxSprite;
	static var _syringe:FlxSprite;
	static var _syringeExists:Bool = false;
	static var _bossExists:Bool = true;
	
	override public function create():Void
	{

		//Load sounds 
		 _jumpHit = FlxG.sound.load(AssetPaths.JumpHit__wav);
		 _introduction = FlxG.sound.load(AssetPaths.Introduction__wav);
		 _blueFound = FlxG.sound.load(AssetPaths.BlueColorFound__wav);
		 _yellowFound = FlxG.sound.load(AssetPaths.YellowColorFound__wav);
		 _redFound = FlxG.sound.load(AssetPaths.RedColorFound__wav);
		 _blueReturned = FlxG.sound.load(AssetPaths.BlueColorReturned__wav);
		 _yellowReturned = FlxG.sound.load(AssetPaths.YellowColorReturned__wav);
		 //_redReturned = FlxG.sound.load(AssetPaths.RedColorReturned__mp3);
		 _begin = FlxG.sound.load(AssetPaths.GradyBeginLine__wav);

		if (FlxG.sound.music == null) // don't restart the music if it's already playing
		{
		    FlxG.sound.playMusic(AssetPaths.Colors0__wav, 1, true);
		}

		super.create();

		//Create the player
		_player = new Player();

		//Create the element sprite
		_element = new FlxSprite();
		_element.loadGraphic(AssetPaths.Red__png,false);
		_element.x = 100;
		_element.y = 500;

		//Create enemies
		_grpEnemies = new FlxTypedGroup<Enemy>();
		// _grpEnemies.add(new EnemyFly());
		// _grpEnemies.add(new EnemyGround());

		//Create background
		_background = new FlxSprite();
		_background.loadGraphic(AssetPaths.bgColor__png,false,3840,1792);

		//Create machine sprite and place it
		_machine = new FlxSprite();
		_machine.loadGraphic(AssetPaths.machine__png,false);
		_machine.x = 3500;
		_machine.y = 1537;

		//Setup the camera
		FlxG.camera.setScrollBounds(0, 0, 3840, 1792);
		FlxG.camera.follow(_player, PLATFORMER, 1);

		//Load the tilemap

 		_map = new FlxOgmoLoader(AssetPaths.Stage00__oel);
 		_mWalls = _map.loadTilemap(AssetPaths.platforms__png, 64, 64, "Platform");
 		//_bg = _map.loadTilemap(AssetPaths.background__png, 64, 64, "Background");
 		_bg1 = new FlxSprite();
 		_bg1.loadGraphic(AssetPaths.background1N__png,false);
 		_bg1.x = 2496;
		_bg1.y = 1080;
 		//_bg2 = _map.loadTilemap(AssetPaths.machine__png, 8, 8, "Interactive");
 		_mWalls.follow();
 		// _mWalls.setTileProperties(2, FlxObject.ANY);
 		// _mWalls.setTileProperties(3, FlxObject.ANY);

 		//_map.loadEntities(placeEntities, "Player");

 		//add(_bg);
 		add(_background);
 		add(_bg1);
 		//add(_bg2);
 		add(_mWalls);
 		add(_grpEnemies);
 		add(_machine);
 		add(_player);
 		add(_element);

 		intro(); //Play the intro sequence

 		// _phase = 3;
 		// switchPhase(_phase);
	}

	override public function update(elapsed:Float):Void
	{
		if(_boss.y <= 800 && !_syringeExists && _bossExists) {
			_phase = 1;
 			switchPhase(_phase);
 			_introduction.play();
			_syringe = new FlxSprite();
 			_syringe.loadGraphic(AssetPaths.Syringe__png,false);
 			_syringe.y = 800;
 			_syringe.x = 2880;
			dropSyringe();
			_syringeExists = true;
			_boss.kill();
			FlxG.camera.flash(FlxColor.WHITE,1);
			_bossExists = false;
		}

		if(_syringeExists) {
			if(_syringe.y >= _player.y - 100) {
				_player._haveSyringe = true;
				_syringe.kill();
				FlxG.camera.flash(FlxColor.WHITE,1);
				_syringeExists = false;
			}
		}

		FlxG.collide(_player, _mWalls);
		FlxG.collide(_grpEnemies, _mWalls);
		FlxG.collide(_player,_grpEnemies,checkKill);
		FlxG.overlap(_player,_element,prepareNextPhase);
		FlxG.overlap(_player,_machine,changePhase);
		super.update(elapsed);
	}

	// private function placeEntities(entityName:String, entityData:Xml):Void
	// {
 //     	var x:Int = Std.parseInt(entityData.get("x"));
 //     	var y:Int = Std.parseInt(entityData.get("y"));
 //     	if (entityName == "player") {
 //        	_player.x = x;
 //        	_player.y = y;
 //    	}
 // 	}

 	private function checkKill(Player:FlxObject, Enemy:Enemy):Void { //Function to check if the player is killed or has defeated an enemy
	 	if(_player.justTouched(FlxObject.DOWN) && _player.alive && Enemy.alive) {
	 		Enemy.die();
	 		_jumpHit.play();
	 		_player.velocity.y = -300;
	 		}
	 		else if(_player.alive && Enemy.alive){
	 			_player.die();
	 		}
 	}

 	public function switchPhase(Phase:Int) { //Function handles loading new assets on stage switching

 		//Destroy any reamining enemies
 		for(enemy in _grpEnemies) {
 			enemy.destroy();
 		}

 		//_bg1.destroy();
 		_element.kill();
 		_mWalls.destroy();
 		_machine.destroy();
 		_machine = new FlxSprite();
 		_machine.x = 3500;
		_machine.y = 1537;

 		if (Phase == 0) {

 			_map = new FlxOgmoLoader(AssetPaths.Stage00__oel);
 			_mWalls = _map.loadTilemap(AssetPaths.platforms__png, 64, 64, "Platform");
 			_bg1.loadGraphic(AssetPaths.background1N__png,false);

 			_machine.loadGraphic(AssetPaths.machine__png,false);
 			_background.loadGraphic(AssetPaths.bgColor__png,false,3840,1792);
 			FlxG.sound.playMusic(AssetPaths.AllColors__wav, 1, true);

 		}

 		if (Phase == 1) {

 			_map = new FlxOgmoLoader(AssetPaths.Stage01__oel);
 			_mWalls = _map.loadTilemap(AssetPaths.platforms__png, 64, 64, "Platform");
 			_bg1.loadGraphic(AssetPaths.background2N__png,false);

 			_machine.loadGraphic(AssetPaths.machine1__png,false);
 			_background.loadGraphic(AssetPaths.backgroundGrey__png,false,3840,1792);
 			_element.loadGraphic(AssetPaths.Blue__png,false);

 			//Add enemies
 			_grpEnemies.add(new EnemyGround());
 			_grpEnemies.add(new EnemyGround(1650,500,1650,1700));

 		}

 		if (Phase == 2) {

 			_map = new FlxOgmoLoader(AssetPaths.Stage02__oel);
 			_mWalls = _map.loadTilemap(AssetPaths.platforms__png, 64, 64, "Platform");
 			_bg1.loadGraphic(AssetPaths.background3N__png,false);

 			_machine.loadGraphic(AssetPaths.machine2__png,false);
 			_background.loadGraphic(AssetPaths.backgroundBlue__png,false,3840,1792);
 			_element.loadGraphic(AssetPaths.Yellow__png,false);

 			//Add enemies
 			_grpEnemies.add(new EnemyGround(650,1202,700,800));
 			_grpEnemies.add(new EnemyGround(750,300,700,800));
 			_grpEnemies.add(new EnemyFly(2980,850,3100,3350));
 			FlxG.sound.playMusic(AssetPaths.Color1__wav, 1, true);

 			_blueReturned.play();
 		}

		if (Phase == 3) {

 			_map = new FlxOgmoLoader(AssetPaths.Stage03__oel);
 			_mWalls = _map.loadTilemap(AssetPaths.platforms__png, 64, 64, "Platform");
 			_bg1.loadGraphic(AssetPaths.background4N__png,false);

 			_machine.loadGraphic(AssetPaths.machine3__png,false);
 			_background.loadGraphic(AssetPaths.backgroundYellow__png,false,3840,1792);
 			_element.loadGraphic(AssetPaths.Red__png,false);

			_grpEnemies.add(new EnemyFly(1150,790,1150,1350));
 			_grpEnemies.add(new EnemyFly(2600,850,2600,2800));
 			_grpEnemies.add(new EnemyFly(1600,1350,1600,1750));
 			FlxG.sound.playMusic(AssetPaths.Color2__wav, 1, true);

 			_element.x = 250;
			_element.y = 300;

			_yellowReturned.play();

 		}

 		if (Phase == 4) {
 			_map = new FlxOgmoLoader(AssetPaths.Stage00__oel);
 			_mWalls = _map.loadTilemap(AssetPaths.platforms__png, 64, 64, "Platform");
 			_bg1.loadGraphic(AssetPaths.background1N__png,false);

 			_machine.loadGraphic(AssetPaths.machine__png,false);
 			_background.loadGraphic(AssetPaths.bgColor__png,false,3840,1792);
 			FlxG.sound.playMusic(AssetPaths.AllColors__wav, 1, true);

 			_grpEnemies.add(new BigBoss());

 			_begin.play();
 		}

 		if (Phase == 5) {
 			FlxG.switchState(new MenuState());
 		}

 		//_player.changeColor(Phase);
 		add(_background);
 		_bg1.draw();
 		add(_mWalls);
 		_mWalls.updateBuffers();
 		add(_machine);
		add(_player);
		_player.draw();
		_element.revive();
 	}

 	function prepareNextPhase(Player:FlxObject,Element:FlxSprite):Void { //Pick up the element and advance the phase counter
 		_element.kill();
 		_phase++;
 		_player.changeColor(_phase);
 		if(_phase==2) {
 			_blueFound.play();
 		}
 		else if(_phase == 3) {
 			_yellowFound.play();
 		}
 		else if(_phase == 4) {
 			_redFound.play();
 		}
 	}

 	function changePhase(Player:FlxObject,Machine:FlxSprite):Void { //Triggers stage change
 		switchPhase(_phase);
 	}

 	function intro():Void {
 		_player._animationLock = true;
 		_boss = new FlxSprite();
 		var spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/BigBadAll.png","assets/images/BigBadAll.json");
		_boss.frames = spritesheet;
		_boss.animation.addByPrefix("Laugh","BigBadLaugh_",2,true,true,false);

		_boss.x = 3080;
		_boss.y = 1500;
		add(_boss);

		_boss.animation.play("Laugh");

		_boss.acceleration.y = -200;
 	}

 	function dropSyringe():Void {
 		_syringe.scale.set(0.8,0.8);
 		add(_syringe);
 		_syringe.acceleration.y = 200;
 	}
}
