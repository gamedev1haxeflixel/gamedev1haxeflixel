package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import flixel.system.FlxSound;

class Player extends FlxSprite{
	
	var speed:Float = 200;
	var _rot:Float = 0;
	var _up:Bool = false;
	var _down:Bool = false;
	var _left:Bool = false;
	var _right:Bool = false;
	public var _animationLock:Bool = true;
	public var _haveSyringe:Bool = false;
	var _gameStart:Bool = false;

	var _jumpSound:FlxSound;

	var spritesheet:FlxAtlasFrames;

	public function new(){
		super();

		//Load sounds 
		 _jumpSound = FlxG.sound.load(AssetPaths.Jumpsound__wav);

		//Change sprite size
		scale.set(0.8,0.8);

		spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/PlayerAll.png","assets/images/PlayerAll.json");
		frames = spritesheet;
		animation.addByPrefix("PlayerRun","PlayerRun_",20,true,true,false);
		animation.addByPrefix("PlayerIdle","PlayerIdle",20,true,true,false);
		animation.addByPrefix("PlayerJump","PlayerJump_",5,false,true,false);
		animation.addByPrefix("PlayerDeath","PlayerDeath_",10,false,true,false);
		animation.addByPrefix("PlayerTop","PlayerJump_Top",1,false,true,false);
		animation.addByPrefix("Naked","PlayerNaked",1,true,true,false);

		width = 35;
		height = 125;
		offset.set(45,9);

		x = 2880;
		y = 1500;

		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		//animation.add("walk", [0,1,0,2], 10, true);
		drag.x = 1000;
		acceleration.y = 1800;
		maxVelocity.set(400, 1000);

		facing = FlxObject.LEFT;
	}

	override public function update(elapsed:Float):Void{
		if(!_haveSyringe && !_gameStart) {
			animation.play("Naked");
		}
		else if(_haveSyringe && !_gameStart){
			animation.stop();
			_animationLock = false;
			_gameStart = true;
		}

		poll();
		movement();
		jump();
		animate();

		if(!alive && animation.finished) {
			respawn();
		}
		super.update(elapsed);
	}
	
	function poll():Void{
		if(!_animationLock) {
			_up = FlxG.keys.anyJustPressed([UP, W, SPACE]);
			_down = FlxG.keys.anyPressed([DOWN, S]);
			_left = FlxG.keys.anyPressed([LEFT, A]);
			_right = FlxG.keys.anyPressed([RIGHT, D]);
		}		
	}

	function movement():Void{
		acceleration.x=0;

		if (_up || _down || _left || _right){
			if (_left){
				_rot = 180;
				facing  = FlxObject.RIGHT;
				acceleration.x -= drag.x;
			}
			else if (_right){
				_rot = 0;
				facing = FlxObject.LEFT;
				acceleration.x += drag.x;
			}
		}
	}
 
	function jump():Void {
		if(_up && velocity.y < 10 && velocity.y > -10) {
			//y -= 1;
			//animation.play("PlayerJump");
			_jumpSound.play();
			velocity.y = -900;
		}
	}

	function animate(): Void { //Determine which animation to play
		if(!_animationLock) {
			if(velocity.x != 0 && velocity.y == 0 && isTouching(FlxObject.DOWN)) {
				animation.play("PlayerRun");
			}
			else if(velocity.y < 0) {
				animation.play("PlayerTop");
			}
			else if(velocity.y == 0 && !isTouching(FlxObject.DOWN)) {
				animation.play("PlayerTop");
			}
			else if(velocity.y > 0) {
				animation.play("PlayerTop");
			}
			else if(velocity.x == 0 && velocity.y == 0 && isTouching(FlxObject.DOWN)){
				animation.play("PlayerIdle");
			}
		}
	}

	public function die(): Void {
		_up = false;
		_down = false;
		_left = false;
		_right = false;

		velocity.x = 0;
		acceleration.x = 0;

		if(alive) {
			_animationLock = true;
			alive  = false;

			animation.play("PlayerDeath");
		}
	}

	function respawn(): Void {
		x = 2880;
		y = 1500;

		alive  = true;
		_animationLock = false;
	}

	public function changeColor(Phase:Int) { //Determines what color palette the player currently has

		animation.remove("PlayerRun");
		animation.remove("PlayerIdle");
		animation.remove("PlayerJump");
		animation.remove("PlayerDeath");
		animation.remove("PlayerTop");

		if(Phase == 0) { //Final color (red)
		spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/RedPlayerAll.png","assets/images/RedPlayerAll.json");
		frames = spritesheet;
		animation.addByPrefix("PlayerRun","RedPlayerRun_",20,true,true,false);
		animation.addByPrefix("PlayerIdle","RedPlayerIdle",20,true,true,false);
		animation.addByPrefix("PlayerJump","RedPlayerJump_",20,false,true,false);
		animation.addByPrefix("PlayerDeath","RedPlayerDeath_",10,false,true,false);
		animation.addByPrefix("PlayerTop","RedPlayerJump_Top",1,false,true,false);
		}
		if(Phase == 1) { //No colors (grey)
		spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/PlayerAll.png","assets/images/PlayerAll.json");
		frames = spritesheet;
		animation.addByPrefix("PlayerRun","PlayerRun_",20,true,true,false);
		animation.addByPrefix("PlayerIdle","PlayerIdle",20,true,true,false);
		animation.addByPrefix("PlayerJump","PlayerJump_",20,false,true,false);
		animation.addByPrefix("PlayerDeath","PlayerDeath_",10,false,true,false);
		animation.addByPrefix("PlayerTop","PlayerJump_Top",1,false,true,false);
		}
		if(Phase == 2) { //First color (Blue)
		spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/BluePlayerAll.png","assets/images/BluePlayerAll.json");
		frames = spritesheet;
		animation.addByPrefix("PlayerRun","BluePlayerRun_",20,true,true,false);
		animation.addByPrefix("PlayerIdle","BluePlayerIdle",20,true,true,false);
		animation.addByPrefix("PlayerJump","BluePlayerJump_",20,false,true,false);
		animation.addByPrefix("PlayerDeath","BluePlayerDeath_",10,false,true,false);
		animation.addByPrefix("PlayerTop","BluePlayerJump_Top",1,false,true,false);
		}
		if(Phase == 3) { //Second color (yellow)
		spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/YellowPlayerAll.png","assets/images/YellowPlayerAll.json");
		frames = spritesheet;
		animation.addByPrefix("PlayerRun","YellowPlayerRun_",20,true,true,false);
		animation.addByPrefix("PlayerIdle","YellowPlayerIdle",20,true,true,false);
		animation.addByPrefix("PlayerJump","YellowPlayerJump_",20,false,true,false);
		animation.addByPrefix("PlayerDeath","YellowPlayerDeath_",10,false,true,false);
		animation.addByPrefix("PlayerTop","YellowPlayerJump_Top",1,false,true,false);
		}
		if(Phase == 4 ) { //Final color (red)
			// spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/RedPlayerAll.png","assets/images/RedPlayerAll.json");
			// frames = spritesheet;
			// animation.addByPrefix("PlayerRun","RedPlayerRun_",20,true,true,false);
			// animation.addByPrefix("PlayerIdle","RedPlayerIdle",20,true,true,false);
			// animation.addByPrefix("PlayerJump","RedPlayerJump_",20,false,true,false);
			// animation.addByPrefix("PlayerDeath","RedPlayerDeath_",10,false,true,false);
			// animation.addByPrefix("PlayerTop","RedPlayerJump_Top",1,false,true,false);

			spritesheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/YellowPlayerAll.png","assets/images/YellowPlayerAll.json");
			frames = spritesheet;
			animation.addByPrefix("PlayerRun","YellowPlayerRun_",20,true,true,false);
			animation.addByPrefix("PlayerIdle","YellowPlayerIdle",20,true,true,false);
			animation.addByPrefix("PlayerJump","YellowPlayerJump_",20,false,true,false);
			animation.addByPrefix("PlayerDeath","YellowPlayerDeath_",10,false,true,false);
			animation.addByPrefix("PlayerTop","YellowPlayerJump_Top",1,false,true,false);
		}

		x -= 10;
		scale.set(0.8,0.8);
		width = 35;
		height = 125;
		offset.set(45,9);

		draw();

		scale.set(0.8,0.8);
		width = 35;
		height = 125;
		offset.set(45,9);
	}
}